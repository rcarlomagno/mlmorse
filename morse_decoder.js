var _ = require('lodash');
var morseMapping = require('./morse_mapping');

var decodeBits2Morse = (bits) => {
    
    //busco minimo valor del dot (1 11 111 1111....)
    counterQty = 0;
    do{
        counterQty++;
        minDot = '0' + Array(counterQty + 1).join('1') + '0';
    }while(bits.indexOf(minDot) < 0)

    //console.log(minDot);
    let minDotQty = minDot.match(/1/g).length;
    //console.log(minDotQty);
    
    //por esquema morse, el dash tiene una duración aproximada de 3 veces el dot
    thresholdDash = minDotQty * 3;
    //todo lo que sea cadena de 1 y su cantidad sea mayor o igual a thresholdDash es dash

    let getSymbolFromBinChar = (symbols) => {
        return _.map(symbols.match(/1+/g), tone => {
            return (tone.match(/1/g).length < thresholdDash) ? '.' : '-';
        }).join('');
    }

    //no hay ningun patron como para detectar espacios entre letras y palabras, ya que en el ejemplo
    //a veces la separacion entre letras es 0000000 y luego separa entre palabras con 0000
    //cuando no deberia pasar eso, en todo caso la separacion entre palabras tiene que ser mayor a la de letras
    //no se si es un error de la cadena de bits de ejemplo en el enunciado
    //no se cumple la parta del enunciado que dice "El mismo concepto aplica a las pausas para la interpretación de la separación entre letras, palabras y finalización del mensaje."
    //por lo tanto no hay separacion entre palabras en la decodificacion


    var binCharacters = _.split(bits.replace(/0{4,}/gm, " "), " ").filter(e => e != '');
    //console.log(binCharacters);
    var symbols = binCharacters.map(getSymbolFromBinChar);
    //console.log(symbols);
    
    //var characters = symbols.map(symbol => morseMapping.morse2Char[symbol]);
    //console.log(characters);
    //var finalSentence = characters.join('');
    
    return symbols;
}

var translate2Human = (morseCode) => {
    var characters = morseCode.map(symbol => morseMapping.morse2Char[symbol]);
    var finalSentence = characters.join('');
    return finalSentence;
}

var translate2Morse = (text) => {
    var symbols = text.split('').map(character => morseMapping.char2Morse[character]);
    var finalSentence = symbols.join(' ');
    return finalSentence;
}


//console.log(decodeBits2Morse('000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000'));
//console.log(translate2Human('.... --- .-..      .- -- . .-.. ..'));

module.exports = {
    decodeBits2Morse,
    translate2Human,
    translate2Morse
}